#!/bin/bash python3
import os
from os import path
import shutil
import sys

def timeStamped(num, fmt='{begin}-%Y-%m-%d-%H-%M-%S_{num}'):
    import datetime
    return datetime.datetime.now().strftime(fmt).format(num=num, begin='tf2-conf')
tf2_folder = r"e:\data\steam\steamapps\common\Team Fortress 2"
backup_location = r"e:\data\backups\tf2_conf"
folders_rel = [
    "tf\\cfg",
    "tf\\custom",
    "tf\\materials",
    "tf\\resource"

]
folders_abs = [path.join(tf2_folder, x) for x in folders_rel]
for folder in folders_rel:
    if path.exists(path.join(tf2_folder, folder)) is False:
        answer = input(f"could not find {folder}. Hit enter to continue, or input any key and hit enter to stop")
        if answer == '':
            print(f"ABORTING\n{folders_abs}")

current_backups = list(os.listdir(backup_location))
numbers = [x.split('_')[-1] for x in current_backups]
numbers.sort(key=int)
new_number = numbers[-1]
new_filename = timeStamped(num=new_number)
for backup,current in [[path.join(backup_location,new_filename, f), path.join(tf2_folder, f)] for f in folders_rel]:
    shutil.copytree(current, backup)
    print(backup)
